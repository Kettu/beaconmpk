//
//  AppDelegate.m
//  BeaconMpk
//
//  Created by Jakub Mazur on 2/8/14.
//  Copyright (c) 2013 Kettu Jakub Mazur. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.nextStopsArray = @[@"Św.Wawrzyńca",@"Miodowa",@"Starowiślna",@"Poczta Główna",@"Dworzec Główny",@"Dworzec Główny Zachód",@"Politechnika"];
    self.tramDestination = @"Krowodrza Górka";
//    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
//    localNotif.fireDate = [NSDate dateWithTimeIntervalSince1970:([[NSDate date] timeIntervalSince1970] + 30)];
//    localNotif.timeZone = [NSTimeZone defaultTimeZone];
//    localNotif.alertBody = @"2 minuty do odjazdu!";
//    localNotif.alertAction = @"Przeciągnij aby odczytać!";
//    localNotif.soundName = UILocalNotificationDefaultSoundName;
//    
//    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:@"someValue" forKey:@"someKey"];
//    localNotif.userInfo = infoDict;
//    
//    [[UIApplication sharedApplication] scheduleLocalNotification:localNotif];
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
