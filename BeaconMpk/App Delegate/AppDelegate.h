//
//  ESTAppDelegate.h
//  BeaconMpk
//
//  Created by Jakub Mazur on 2/8/14.
//  Copyright (c) 2013 Kettu Jakub Mazur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) NSMutableArray *nextStopsArray;
@property (strong,nonatomic) NSString *tramDestination;

@end
