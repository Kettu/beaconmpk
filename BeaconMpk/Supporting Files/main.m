//
//  main.m
//  BeaconMpk
//
//  Created by Jakub Mazur on 2/8/14.
//  Copyright (c) 2013 Kettu Jakub Mazur. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
