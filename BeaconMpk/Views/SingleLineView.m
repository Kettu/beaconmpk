//
//  SingleLineView.m
//  BikonMpk
//
//  Created by Jakub Mazur on 08.02.2014.
//  Copyright (c) 2014 Estimote. All rights reserved.
//

#import "SingleLineView.h"

@implementation SingleLineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)addScrollViewWithLineNumber:(NSInteger)line andHours:(NSArray*)hours {
    UIScrollView *scView = [[UIScrollView alloc] initWithFrame:CGRectMake(60, 0, 260, 44)];
    [scView setContentOffset:CGPointMake(44, 320)];
    [self addSubview:scView];
    [self addLabelsFromDatabase:hours];
    [self addColorLabelWithLineNumber:line];
}

-(void)addColorLabelWithLineNumber:(int)number {
    UIView *trainNumber = [[UIView alloc] initWithFrame:CGRectMake(4, 4, 44, 36)];
    trainNumber.layer.cornerRadius = 8;
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 44, 36)];
    [trainNumber setBackgroundColor:[UIColor colorWithRed:0.08 green:0.46 blue:0.83 alpha:1.0]];
    [textLabel setText:[NSString stringWithFormat:@"%i",number]];
    [textLabel setTextAlignment:NSTextAlignmentCenter];
    [textLabel setTextColor:[UIColor whiteColor]];
    [trainNumber addSubview:textLabel];
    
    [self addSubview:trainNumber];
}

-(NSString*)convertDateUsingFormatter:(NSDate*)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    return [dateFormatter stringFromDate:date];
}

-(void)addLabelsFromDatabase:(NSArray*)database {
    float distance = 60;
    float margin = 10;
    
    for(NSDate *dater in database) {
        UILabel *labal = [[UILabel alloc] initWithFrame:CGRectMake(distance+margin, 10, 60+distance, 20)];
        [labal setText:[self convertDateUsingFormatter:dater]];
        [self addSubview:labal];
        distance += 60;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
