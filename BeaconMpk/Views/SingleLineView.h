//
//  SingleLineView.h
//  BikonMpk
//
//  Created by Jakub Mazur on 08.02.2014.
//  Copyright (c) 2014 Estimote. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleLineView : UIView

@property (strong,nonatomic) UIView *lineNumberView;

-(void)addScrollViewWithLineNumber:(NSInteger)line andHours:(NSArray*)hours;

@end
