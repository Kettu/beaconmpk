//
//  DetailViewController.m
//  BikonMpk
//
//  Created by Jakub Mazur on 08.02.2014.
//  Copyright (c) 2014 Estimote. All rights reserved.
//

#import "DetailViewController.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    AppDelegate *dlg = [[UIApplication sharedApplication] delegate];
    NSString *str = [NSString stringWithFormat:@"Linia numer trzy.Kierunek Krowodrza Górka. Następny przystanek %@",dlg.nextStopsArray[0]];
    
    self.lineNumber.accessibilityLabel = str;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [((AppDelegate*)[UIApplication sharedApplication].delegate).nextStopsArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"ItemCell"];
    NSString *strow = ((AppDelegate*)[UIApplication sharedApplication].delegate).nextStopsArray[indexPath.row];
    [cell.textLabel setText:strow];
    return cell;
}

-(void)speedWithTextt:(NSString*)text {
    AVSpeechUtterance *utterance = [AVSpeechUtterance
                                    speechUtteranceWithString:text];
    AVSpeechSynthesizer *synth = [[AVSpeechSynthesizer alloc] init];
    [utterance setRate:0.5];
    
    [synth speakUtterance:utterance];
}

-(void)updateStops {
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    NSMutableArray *ntgoarr = [[NSMutableArray alloc] initWithArray:app.nextStopsArray];
    NSString *first = [app.nextStopsArray firstObject];
    [self.nextStop setText:ntgoarr[1]];
    NSString *str = [NSString stringWithFormat:@"Przystanek %@. Następny przystanek %@. Linia numer trzy. Kierunek Krowodrza Górka.",first,ntgoarr[1]];
    MainViewController *parentVC = (MainViewController*)self.parentViewController;
    self.nextStop.accessibilityLabel = ntgoarr[1];
    
    self.lineNumber.accessibilityLabel = str;
    //dispatch_async(dispatch_get_main_queue(), ^{
        [self speedWithTextt:str];
    //});
    
    [ntgoarr removeObjectAtIndex:0];
    app.nextStopsArray = ntgoarr;
    [self.currentStop setNeedsDisplay];
    [self.tableView reloadData];
    
    
}

@end
