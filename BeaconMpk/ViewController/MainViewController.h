//
//  MainViewController.h
//  BeaconMpk
//
//  Created by Jakub Mazur on 2/8/14.
//  Copyright (c) 2013 Kettu Jakub Mazur. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTBeaconManager.h"
#import "SingleLineView.h"
#import "DetailViewController.h"

@interface MainViewController : UIViewController <ESTBeaconDelegate,ESTBeaconManagerDelegate>

@property (nonatomic, strong) ESTBeaconManager* beaconManager;
@property (nonatomic, strong) UIImageView*      positionDot;
@property (nonatomic, strong) ESTBeacon*        closestBeacon;
@property (assign) BOOL isConnectingToBeacon;
@property (assign) BOOL getToTheStop;
@property (nonatomic,strong) ESTBeacon*         tramBeacon;
@property (weak, nonatomic) IBOutlet UIImageView *tramImageView;
@property (weak, nonatomic) IBOutlet UILabel *closeNumberLine;
@property (weak, nonatomic) IBOutlet UILabel *farNumberLine;
@property (weak, nonatomic) IBOutlet UIImageView *farImage;
@property (weak, nonatomic) IBOutlet UIView *generalInformationView;
@property (assign) BOOL alreadyUpdated;

@property (nonatomic) float dotMinPos;
@property (nonatomic) float dotRange;
@property (strong,nonatomic) NSArray *databaseArray;

@property (strong,nonatomic) NSTimer *timer4Trams;

@property (strong,nonatomic) UILabel *debugLabel;

-(void)speedWithText:(NSString*)text;

@end
