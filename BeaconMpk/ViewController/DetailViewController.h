//
//  DetailViewController.h
//  BikonMpk
//
//  Created by Jakub Mazur on 08.02.2014.
//  Copyright (c) 2014 Estimote. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTBeaconManager.h"

@interface DetailViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,ESTBeaconDelegate,ESTBeaconManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong,nonatomic) UILabel *debugLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentStop;
@property (weak, nonatomic) IBOutlet UILabel *tramDestination;
@property (weak, nonatomic) IBOutlet UILabel *lineNumber;
@property (weak, nonatomic) IBOutlet UILabel *nextStop;


-(void)updateStops;

@end
