//
//  MainViewController.m
//  BeaconMpk
//
//  Created by Jakub Mazur on 2/8/14.
//  Copyright (c) 2013 Kettu Jakub Mazur. All rights reserved.
//

#import "MainViewController.h"
#import <AVFoundation/AVFoundation.h>

#define DOT_MIN_POS 120
#define DOT_MAX_POS screenHeight - 70;

@implementation MainViewController

#pragma mark - Manager setup

- (void)setupManager {
    // create manager instance
    self.beaconManager = [[ESTBeaconManager alloc] init];
    self.beaconManager.delegate = self;
    
    // create sample region object (you can additionaly pass major / minor values)
    ESTBeaconRegion* region = [[ESTBeaconRegion alloc] initWithProximityUUID:ESTIMOTE_PROXIMITY_UUID identifier:@"EstimoteSampleRegion"];
    
    // start looking for estimote beacons in region
    // when beacon ranged beaconManager:didRangeBeacons:inRegion: invoked
    [self.beaconManager startRangingBeaconsInRegion:region];

}

-(void)loadData {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://beaconmpk.antyzero.com/?beacon=id%i",[self.closestBeacon.minor integerValue]]]];
        if(data) {
            NSDictionary *myJSON = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            self.databaseArray = [[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil] objectForKey:@"lines"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self addScheduleToPseudoCells];

                self.title = [myJSON objectForKey:@"name"];
                if([[myJSON objectForKey:@"name"] isEqualToString:@"Św.Wawrzyńca"])
                    self.title = @"Świętego Wawrzyńca";
                if(self.getToTheStop && ([self.closestBeacon.distance floatValue] < 3)) {
                    self.getToTheStop = NO;
                    NSMutableString *str = [[NSMutableString alloc] init];
                    if(!self.tramBeacon) {
                        [str appendString:@"Brak tramwajów. "];
                    }
                    [str appendString:@"Przystanek"];
                    if([self.title isEqualToString:@"Świętego Wawrzyńca"]) {
                        [str appendString:@"Świętego Wawrzyńca."];
                    } else if([self.title isEqualToString:@"Plac Bohaterów Getta"]) {
                        [str appendString:@"Plac Bohaterów Getta"];
                    }
                    [str appendString:@" Kierunki: Krowodrza Górka, Bronowice, Dworzec Towarowy."];

                    self.generalInformationView.accessibilityLabel = str;
                    if(!self.presentedViewController) {
                        [self speedWithText:str];
                    }
                }
            });
        }
    });
}

-(void)speedWithText:(NSString*)text {
    AVSpeechUtterance *utterance = [AVSpeechUtterance
                                    speechUtteranceWithString:text];
    AVSpeechSynthesizer *synth = [[AVSpeechSynthesizer alloc] init];
    [utterance setRate:0.5];
    
    [synth speakUtterance:utterance];
}

#pragma mark - ViewController Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(loadData) userInfo:nil repeats:YES];
    
    [self setupManager];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"flare"] forBarMetrics:UIBarMetricsDefault];
}

-(void)dismissEver {
    if(self.presentedViewController) {
        [self dismissViewControllerAnimated:YES completion:^{
            self.closeNumberLine.alpha = 0.0;
            [self.farImage setAlpha:0.0];
            [self loadData];
        }];
    }
}

-(NSArray*)convertMinutesToDate:(NSArray*)arr {
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSUInteger preservedComponents = (NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit);
    date = [calendar dateFromComponents:[calendar components:preservedComponents fromDate:date]];
    NSMutableArray *returnArray = [[NSMutableArray alloc] init];
    for(NSNumber *dateMinutes in arr) {
        NSInteger timestamp = [dateMinutes integerValue] * 60;
        NSDate *myDate = [NSDate dateWithTimeInterval:timestamp sinceDate:date];
        if([myDate compare:[NSDate date]] == NSOrderedDescending) {
            [returnArray addObject:myDate];
        }
    }
    return returnArray;
}

-(void)addScheduleToPseudoCells {
    
    for(UIView *view in [self.view subviews]) {
        if([view isKindOfClass:[SingleLineView class]]) {
            [view removeFromSuperview];
        }
    }
    
    float downValue = 0;
    NSTimeInterval currentDate = [[NSDate date] timeIntervalSince1970];
    for(NSDictionary *dic in self.databaseArray) {
        SingleLineView *sView = [[SingleLineView alloc] initWithFrame:CGRectMake(0, 240+downValue, 320, 44)];
        NSArray *datesFromMinutes = [self convertMinutesToDate:[dic objectForKey:@"minutes"]];
        [sView addScrollViewWithLineNumber:[[dic objectForKey:@"line"] integerValue] andHours:datesFromMinutes];
        [sView setUserInteractionEnabled:YES];
        [sView setIsAccessibilityElement:YES];
        [sView setBackgroundColor:[UIColor whiteColor]];
        
        NSTimeInterval timeInter = 0.0;
        NSTimeInterval timeInter2 = 0.0;
        if([datesFromMinutes count] > 0)
            timeInter = [datesFromMinutes[0] timeIntervalSince1970] - currentDate;
        if([datesFromMinutes count] > 1)
            timeInter2 = [datesFromMinutes[1] timeIntervalSince1970] - currentDate;
        
        sView.accessibilityLabel = [NSString stringWithFormat:@"%@, Odjazd za %.0f minut. Następny za %.0f minut",[dic objectForKey:@"line"],floor(timeInter/60),floor(timeInter2/60)];
        [self.view addSubview:sView];
        downValue+=45;
    }
}

-(void)scheduledTram {
    NSString *str = @"Na przystanku: trzy. Kierunek: Krowodrza Górka.";
    [self speedWithText:str];
    self.generalInformationView.accessibilityLabel = str;
    self.farImage.alpha = 1.0;
    [self.closeNumberLine setText:@"3"];

    [UIView animateWithDuration:0.5 animations:^{
        [self.farImage setFrame:self.tramImageView.frame];
        [self.closeNumberLine setAlpha:1.0];
    }];
}

#pragma mark - ESTBeaconManagerDelegate Implementation

-(void)beaconManager:(ESTBeaconManager *)manager didRangeBeacons:(NSArray *)beacons inRegion:(ESTBeaconRegion *)region {
    
    //closest beacon (bus stop)
    for(ESTBeacon *bc in beacons) {
        if([bc.minor integerValue] != TRAM_BEACON) {
            if(![self.closestBeacon isEqual:bc]) {
                self.getToTheStop = YES;
                [self loadData];
            }
            self.closestBeacon = bc;
            if(!self.title) {
                [self loadData];
            }
            break;
        }
    }
    
    //finding tram
    for(ESTBeacon *bea in beacons) {
        if([bea.minor integerValue] == TRAM_BEACON && bea != self.tramBeacon) {
            self.tramBeacon = bea;
            [self scheduledTram];
        }
    }
    
    //only tram available
    ESTBeacon *bicon = [beacons objectAtIndex:TRAM_BEACON];
    if([bicon.minor integerValue] == TRAM_BEACON && [beacons count] == 1) {
        if(!self.presentedViewController)
            [self performSegueWithIdentifier:@"onTheMoveSegue" sender:nil];
    }
    
    //get rid off the tram
    BOOL found = NO;
    for(ESTBeacon *b in beacons) {
        if([b.minor integerValue] == TRAM_BEACON) {
            found = YES;
        }
    }
    if(!found) {
        self.tramBeacon = nil;
        if(self.presentedViewController)
            [self dismissEver];
    }
    
    if(!self.alreadyUpdated) {
    //mismatch with tableView to stop
        BOOL tramBeaconFound = NO;
        BOOL destinationBeaconFound = NO;
        for(ESTBeacon *bea in beacons) {
            if([bea.minor integerValue] == TRAM_BEACON) {
                tramBeaconFound = YES;
            }
            if([bea.minor integerValue] == VIOLET_DESTINATION_STOP_BEACON) {
                destinationBeaconFound = YES;
            }
        }
        if(tramBeaconFound && destinationBeaconFound) {
            [((DetailViewController*)self.presentedViewController) updateStops];
            [self setAlreadyUpdated:YES];
        }
    }
    
}

@end
